FROM python:3.7.4-slim
WORKDIR /home

RUN apt update
RUN apt-get install git -y
RUN git clone https://gitlab.com/ki.alkiviad/long_poll_py_bot
RUN cd long_poll_py_bot/ && pip install --no-cache-dir -r requirements.txt